/**
 * @type {string[][]}
 */

export const teamsKnown = [
	['Avropack', 'Gəncə (satış)', 'Anbar təsərrüfatı', 'Audit'],
	['Bərdə (satış)', 'Əməliyyatlar', 'Fabrist (İstehsalat)', 'Harika (satış)'],
	['İstehsalçı (İstehsalat)', 'Key Account', 'Keyfiyyətə nəzarət', 'Lənkəran (satış)'],
	['Logistika', 'Maliyyə', 'Marketing', 'Milla (satış)'],
	['Quba (satış)', 'Saatlı (satış)', 'Enerji (satış)', 'Şəki (satış)'],
	['SPL (Tədarük zənciri)', 'Dairy Team (süd şöbəsi)', 'Tamstore', 'Təhlükəsizlik'],
	['Xırdalan (satış)', 'Oven', 'Ellab', 'Avroviski']
];

/**
 * @type {string[]}
 */

export const teamNames = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
