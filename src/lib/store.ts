import { writable } from 'svelte/store';

export const teams: import('svelte/store').Writable<string[]> = writable([]);

export const loading = writable(true);

const disabledElements = writable(new Set());

export default disabledElements;